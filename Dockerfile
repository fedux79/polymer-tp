FROM httpd:2.4
RUN mkdir /var/www/
#COPY ./build/default/ /usr/local/apache2/htdocs/
COPY ./build/default/ /var/www/
COPY ./httpd.conf /usr/local/apache2/conf/httpd.conf
COPY ./httpd-vhosts.conf /usr/local/apache2/conf/extra/httpd-vhosts.conf
